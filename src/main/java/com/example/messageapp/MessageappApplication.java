package com.example.messageapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({ "com.example.messageapp.controlle.WebSocketEventListener","com.example.messageapp.config.WebSocketConfig","com.example.messageapp.controller.ChatController" })
public class MessageappApplication {

	public static void main(String[] args) {
		SpringApplication.run(MessageappApplication.class, args);
	}

}
